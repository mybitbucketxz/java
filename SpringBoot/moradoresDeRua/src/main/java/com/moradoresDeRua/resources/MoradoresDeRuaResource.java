package com.moradoresDeRua.resources;

import com.moradoresDeRua.models.MoradorDeRua;
import com.moradoresDeRua.repository.MoradorDeRuaRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


@Api(value="API REST Moradores de rua")
@RestController
@RequestMapping("/moradoresDeRua")
public class MoradoresDeRuaResource {

    @Autowired
    private MoradorDeRuaRepository mr;


    @ApiOperation(value="Retorna uma lista de moradores de rua")
    @GetMapping(produces = "application/json")
    public @ResponseBody
    Iterable<MoradorDeRua> listaMoradoresDeRua(){
        Iterable<MoradorDeRua> listaEventos = mr.findAll();
        ArrayList<MoradorDeRua> moradoresDeRua = new ArrayList<MoradorDeRua>();
        for(MoradorDeRua moradorDeRua : listaEventos) {
            long codigo = moradorDeRua.getCodigo();
            moradorDeRua.add(linkTo(methodOn(MoradoresDeRuaResource.class).moradorDeRua(codigo)).withSelfRel());
            moradoresDeRua.add(moradorDeRua);
        }
        return moradoresDeRua;
    }

    @ApiOperation(value="Retorna um morador de rua especifico")
    @GetMapping(value="/{codigo}", produces = "application/json")
    public @ResponseBody MoradorDeRua moradorDeRua(@PathVariable(value="codigo") long codigo){
        MoradorDeRua moradorDeRua = mr.findByCodigo(codigo);
        moradorDeRua.add(linkTo(methodOn(MoradoresDeRuaResource.class).listaMoradoresDeRua()).withRel("Lista de moradores de rua"));
        return moradorDeRua;
    }


    @ApiOperation(value="Salva um morador de rua")
    @PostMapping()
    public MoradorDeRua cadastraMoradorDeRua(@RequestBody @Valid MoradorDeRua moradorDeRua){
        return mr.save(moradorDeRua);
    }

    @ApiOperation(value="Deleta um morador de rua")
    @DeleteMapping()
    public MoradorDeRua  deletaMoradorDeRua(@RequestBody MoradorDeRua moradorDeRua){
        mr.delete(moradorDeRua);
        return moradorDeRua;
    }



}
