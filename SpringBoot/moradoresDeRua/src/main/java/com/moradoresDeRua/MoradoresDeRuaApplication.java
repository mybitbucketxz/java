package com.moradoresDeRua;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoradoresDeRuaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoradoresDeRuaApplication.class, args);
	}
}
