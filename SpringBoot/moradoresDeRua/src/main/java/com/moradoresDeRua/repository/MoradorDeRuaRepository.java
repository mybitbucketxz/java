package com.moradoresDeRua.repository;
import com.moradoresDeRua.models.MoradorDeRua;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoradorDeRuaRepository extends  JpaRepository <MoradorDeRua, String>{
    MoradorDeRua findByCodigo(long codigo);

}
