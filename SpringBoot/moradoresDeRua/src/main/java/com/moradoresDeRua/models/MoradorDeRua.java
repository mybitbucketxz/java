package com.moradoresDeRua.models;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class MoradorDeRua extends ResourceSupport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codigo;

    @NotNull
    private String encontradoDia, horarioEncontrado,  localOndeFoiEncontradoNoGoogleMaps, nomeCompletoDoMoradorDeRua,  nomeCompletoDoPai, nomeCompletoDaMae;

    @NotNull
    private String cidadeOrigem, bairroOrigem;

    private String apelido, historiaDeVida, AmigosConhecidos, empresasEmQueTrabalhou, localOndePretendeIr;

    private String imagemDoMoradorDeRuaGrande, imagemDoMoradorDeRuaMedia, imagemDoMoradorDeRuaPequena;

    private int idade;

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getEncontradoDia() {
        return encontradoDia;
    }

    public void setEncontradoDia(String encontradoDia) {
        this.encontradoDia = encontradoDia;
    }

    public String getHorarioEncontrado() {
        return horarioEncontrado;
    }

    public void setHorarioEncontrado(String horarioEncontrado) {
        this.horarioEncontrado = horarioEncontrado;
    }

    public String getLocalOndeFoiEncontradoNoGoogleMaps() {
        return localOndeFoiEncontradoNoGoogleMaps;
    }

    public void setLocalOndeFoiEncontradoNoGoogleMaps(String localOndeFoiEncontradoNoGoogleMaps) {
        this.localOndeFoiEncontradoNoGoogleMaps = localOndeFoiEncontradoNoGoogleMaps;
    }

    public String getNomeCompletoDoMoradorDeRua() {
        return nomeCompletoDoMoradorDeRua;
    }

    public void setNomeCompletoDoMoradorDeRua(String nomeCompletoDoMoradorDeRua) {
        this.nomeCompletoDoMoradorDeRua = nomeCompletoDoMoradorDeRua;
    }

    public String getNomeCompletoDoPai() {
        return nomeCompletoDoPai;
    }

    public void setNomeCompletoDoPai(String nomeCompletoDoPai) {
        this.nomeCompletoDoPai = nomeCompletoDoPai;
    }

    public String getNomeCompletoDaMae() {
        return nomeCompletoDaMae;
    }

    public void setNomeCompletoDaMae(String nomeCompletoDaMae) {
        this.nomeCompletoDaMae = nomeCompletoDaMae;
    }

    public String getCidadeOrigem() {
        return cidadeOrigem;
    }

    public void setCidadeOrigem(String cidadeOrigem) {
        this.cidadeOrigem = cidadeOrigem;
    }

    public String getBairroOrigem() {
        return bairroOrigem;
    }

    public void setBairroOrigem(String bairroOrigem) {
        this.bairroOrigem = bairroOrigem;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getHistoriaDeVida() {
        return historiaDeVida;
    }

    public void setHistoriaDeVida(String historiaDeVida) {
        this.historiaDeVida = historiaDeVida;
    }

    public String getAmigosConhecidos() {
        return AmigosConhecidos;
    }

    public void setAmigosConhecidos(String amigosConhecidos) {
        AmigosConhecidos = amigosConhecidos;
    }

    public String getEmpresasEmQueTrabalhou() {
        return empresasEmQueTrabalhou;
    }

    public void setEmpresasEmQueTrabalhou(String empresasEmQueTrabalhou) {
        this.empresasEmQueTrabalhou = empresasEmQueTrabalhou;
    }

    public String getLocalOndePretendeIr() {
        return localOndePretendeIr;
    }

    public void setLocalOndePretendeIr(String localOndePretendeIr) {
        this.localOndePretendeIr = localOndePretendeIr;
    }

    public String getImagemDoMoradorDeRuaGrande() {
        return imagemDoMoradorDeRuaGrande;
    }

    public void setImagemDoMoradorDeRuaGrande(String imagemDoMoradorDeRuaGrande) {
        this.imagemDoMoradorDeRuaGrande = imagemDoMoradorDeRuaGrande;
    }

    public String getImagemDoMoradorDeRuaMedia() {
        return imagemDoMoradorDeRuaMedia;
    }

    public void setImagemDoMoradorDeRuaMedia(String imagemDoMoradorDeRuaMedia) {
        this.imagemDoMoradorDeRuaMedia = imagemDoMoradorDeRuaMedia;
    }

    public String getImagemDoMoradorDeRuaPequena() {
        return imagemDoMoradorDeRuaPequena;
    }

    public void setImagemDoMoradorDeRuaPequena(String imagemDoMoradorDeRuaPequena) {
        this.imagemDoMoradorDeRuaPequena = imagemDoMoradorDeRuaPequena;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
}
